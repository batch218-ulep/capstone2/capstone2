// DEPENDENCIES: Modules
const express = require("express");
const router = express.Router();


// DEPENDENCIES: Local
//const User =  require("../models/User.js");
const auth = require("../auth.js")
// const userController = require("../controllers/userController.js");
const orderController = require("../controllers/orderController.js");
const cartController = require("../controllers/cartController.js");

const {
	registerUser,
	checkEmail,
	loginUser,
	getUserDetails,
	addAdmin,
	updateAccountInfo,
	changePassword,
	deactivateAccount
} = require("../controllers/userControllers.js");

// ROUTES--------------------------------------------------------------

// USER REGISTRATION
router.post("/register", (request, response) => {
	registerUser(request.body)
	.then(result => response.send(result));
});

// CHECK REGISTRATION EMAIL
router.post("/checkEmail", (request, response) => {
	checkEmail(request.body)
	.then(result => response.send(result));
});

// USER LOGIN
router.post("/login", (request, response) => {
	loginUser(request.body)
	.then(result => response.send(result));
});


// USER CHECKOUT: NON-ADMIN ONLY
router.post("/checkout", auth.verify , (request, response) => {
	let customer = {
		userId: auth.decode(request.headers.authorization).id,
		fullName: auth.decode(request.headers.authorization).fullName,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	orderController.createOrder(customer, request.body)
	.then(result => response.send(result));
});


// USER DETAILS: SINGLE
router.get("/details", auth.verify, (request, response) => {
	getUserDetails(auth.decode(request.headers.authorization).id)
	.then(result => response.send(result));
});


// USER SET AS ADMIN: ADMIN ONLY
router.put("/setAsAdmin/:newAdminUserId", auth.verify, (request, response) => {
	let isAdmin = auth.decode(request.headers.authorization).isAdmin

	addAdmin(isAdmin, request.params.newAdminUserId)
	.then(result => response.send(result));
});


// GET ALL USER'S ORDERS
router.get("/myOrders", auth.verify, (request, response) => {
	let customer = {
		userId: auth.decode(request.headers.authorization).id,
		fullName: auth.decode(request.headers.authorization).fullName,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	orderController.getMyOrders(customer)
	.then(result => response.send(result));
});


// RETRIEVE ALL ORDERS: ADMIN ONLY
router.get("/orders", auth.verify, (request, response) => {
	let isAdmin = auth.decode(request.headers.authorization).isAdmin;

	orderController.getAllOrders(isAdmin)
	.then(result => response.send(result));
});


// ADD TO CART
router.post("/addToCartMany", auth.verify, (request, response) => {
	let customer = {
		userId: auth.decode(request.headers.authorization).id,
		fullName: auth.decode(request.headers.authorization).fullName,
		isAdmin: auth.decode(request.headers.authorization).isAdmin	
	}

	cartController.addToCartMany(customer, request.body)
	.then(result => response.send(result));
});


// VIEW CART
router.get("/viewcart", auth.verify, (request, response) => {
	let customer ={
		fullName: auth.decode(request.headers.authorization).fullName,
		userId: auth.decode(request.headers.authorization).id,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	cartController.viewCart(customer)
	.then(result => response.send(result));
});


// UPDATE ACCOUNT DETAILS
router.put("/updateAccountInfo", auth.verify, (request, response) => {
	let userId = auth.decode(request.headers.authorization).id;

	updateAccountInfo(userId, request.body)
	.then(result => response.send(result));
});


// CHANGE ACCOUNT PASSWORD
router.put("/changePassword", auth.verify, (request, response) => {
	let userId = auth.decode(request.headers.authorization).id;

	changePassword(userId, request.body)
	.then(result => response.send(result));
});


// DEACTIVATE ACCOUNT
router.delete("/deactivate", auth.verify, (request, response) => {
	let userId = auth.decode(request.headers.authorization).id;

	deactivateAccount(userId, request.body)
	.then(result => response.send(result));
});


// ADD TO CART: SINGLE PRODUCT
router.post("/addToCart", auth.verify, (request, response) => {
	let customer = {
		userId: auth.decode(request.headers.authorization).id,
		fullName: auth.decode(request.headers.authorization).fullName
	}

	cartController.addToCart(customer, request.body)
	.then(result => response.send(result));
});


// MODIFY PRODUCT DETAILS IN CART
router.put("/carts/update", (request, response) => {
	cartController.updateCart(request.body)
	.then(result => response.send(result));
});


// CLEAR CART
router.delete("/carts/clearcart", auth.verify, (request, response) => {
	cartController.clearCart(auth.decode(request.headers.authorization).id)
	.then(result => response.send(result));
});





// EXPORT USER ROUTES
module.exports = router;