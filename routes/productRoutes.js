// DEPENDENCIES: Modules
const express = require("express");
const router = express.Router();


// DEPENDENCIES: Local
const Product =  require("../models/Products.js");
const productController =  require("../controllers/productController.js");
const auth = require("../auth.js");


// ROUTES--------------------------------------------------------------

// PRODUCT CREATION: ADMIN ONLY
router.post("/create", auth.verify, (request, response) => {
	const isAdmin = auth.decode(request.headers.authorization).isAdmin;
	productController.addProduct(request.body, isAdmin)
	.then(result => response.send(result));
});


// PRODUCT DISPLAY: ALL ACTIVE
router.get("/active", (request, response) => {
	productController.getActiveProducts()
	.then(result => response.send(result));
});

// PRODUCT DISPLAY: ALL ACTIVE
router.get("/all", (request, response) => {
	productController.getAllProducts()
	.then(result => response.send(result));
});

// PRODUCT DISPLAY: SINGLE PRODUCT
router.get("/:productId", (request, response) => {
	productController.getProductDetails(request.params.productId)
	.then(result => response.send(result));
});


// PRODUCT UPDATE INFO: ADMIN ONLY
router.put("/update/:productId", auth.verify, (request, response) => {
	const isAdmin = auth.decode(request.headers.authorization).isAdmin;
	productController.updateProduct(request.params.productId, isAdmin, request.body)
	.then(result => response.send(result));
});


// PRODUCT ARCHIVE: ADMIN ONLY
router.put("/archive/:productId", auth.verify, (request, response) => {
	const isAdmin = auth.decode(request.headers.authorization).isAdmin;
	productController.archiveProduct(request.params.productId, isAdmin)
	.then(result => response.send(result));
});

// PRODUCT UNARCHIVE: ADMIN ONLY
router.put("/unarchive/:productId", auth.verify, (request, response) => {
	const isAdmin = auth.decode(request.headers.authorization).isAdmin;
	productController.unarchiveProduct(request.params.productId, isAdmin)
	.then(result => response.send(result));
});


// EXPORT PRODUCT ROUTES
module.exports = router;